package com.guatecompras.LicitacionManger.scrap;


import com.guatecompras.LicitacionManger.domain.PageInfo;
import com.guatecompras.LicitacionManger.services.PageInfoService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GetDocument {

    //inyeccion de dependencia
    private final PageInfoService pageInfoService;

    private static final String url = "http://www.guatecompras.gt/concursos/busquedaTexto.aspx?t=muebles";
    private static final int maxPages = 20;
    private static final String newLineSeparator = "\n \n";
    private static final Object[] fileHeader = {"Reference Code ", "Url ", "Description "};
    private static List<String> infoScrappe = new ArrayList<String>();
    private static ArrayList<String> licitationsUrl = new ArrayList<String>();


    public GetDocument(PageInfoService pageInfoService) {
        this.pageInfoService = pageInfoService;
    }

    public void selectAllLicitations() {

        //Check if the connection with the page to scrape could be completed
        int statusConnection = getStatusConectionCode(url);

        if ((statusConnection == 200)) {

            System.out.println("Connection with URL established");
            System.out.println(" ");

            //Get the web page on a document
            Document document = getHtmlDocument(url);

            //Select the elements of the documents


            Elements links = document.select("a[href^=/concursos/consultaConcurso]");

            /* The List links of Elements has all the links of the page where are all the URL's of the licitations
            * but there are 2 URL that are the same by licitation on the page. We have to select only one, and
            * for this reason we have to select only one with the following <if> within the <foreach> cycle
            * */
            int count = 0;
            for (Element link : links) {

                if (count == 0) {
                    // System.out.println(link.attr("abs:href"));

                    //The following stores only the links that we need on an Arraylist
                    licitationsUrl.add(link.attr("abs:href"));

                    count++;

                } else {
                    if (count == 1) {
                        count = 0;
                    }
                }
            }
        }

    }

    /**
     * Check if the connection with the page to scrape could be completed
     *
     * @return an int that represent the status connection code with the URL to Scrape
     * Status code 200 = ok
     * @
     */
    private int getStatusConectionCode(String url) {

        Connection.Response response = null;

        try {

            response = Jsoup.connect(url).userAgent("Mozilla").timeout(100000).ignoreHttpErrors(true).execute();
        } catch (IOException ex) {
            System.out.println("Error obtaining the status of the web " + ex.getMessage());
        }

        return response.statusCode();
    }

    /**
     * Generates a CSV Document with all the information of the URL to scrape
     *
     * @param url of the web to scrape
     * @return a document with all the HTML of the web scraped
     */
    private Document getHtmlDocument(String url) {

        Document doc = null;

        try {
            doc = Jsoup.connect(url).userAgent("Mozilla").timeout(100000).get();

        } catch (IOException ex) {
            System.out.println("There was an error storing the HTML on a document " + ex.getMessage());
        }

        return doc;
    }

    /**
     * Gets all the information within the URL of the links of the Arraylist of licitation
     */
    public void scrappeInfoLicitation() {

        String licitationCode = "";
        String description = "";
        String modalidad = "";
        String categoria = "";
        String tipoConcurso = "";
        String entidad = "";
        String tipoEntidad = "";
        String unidadCompradora = "";
        String recepcionOfertas = "";
        Date fechaPublicacion = new Date();
        Date fechaCiereOferta = new Date();
        String tipoProceso = "";
        Date fechaFinalizacion = new Date();
        String estatus = "";

        for (String info : licitationsUrl) {

            Document licitationinfo = getHtmlDocument(info);

            Elements e = licitationinfo.getElementsByClass("EtiquetaInfo");

            ArrayList<PageInfo> arrayPages = new ArrayList<PageInfo>();


            int counter = 0;
            //This loop select all the information to scrape and put them on an arraylist
            for (Element data : e) {

                switch (counter) {

                    case 0:

                        licitationCode = data.text();
                        counter++;
                        break;

                    case 1:

                        description = data.text();
                        counter++;
                        break;

                    case 2:

                        modalidad = data.text();
                        counter++;
                        break;

                    case 3:
                        categoria = data.text();
                        counter++;
                        break;

                    case 4:
                        tipoConcurso = data.text();
                        counter++;
                        break;

                    case 5:
                        entidad = data.text();
                        counter++;
                        break;

                    case 6:

                        tipoEntidad = data.text();
                        counter++;
                        break;

                    case 7:

                        unidadCompradora = data.text();
                        counter++;
                        break;

                    case 8:

                        recepcionOfertas = data.text();
                        counter++;
                        break;

                    case 9:

                        Date datePublication = stringToDate(formatStringDate(data.text()));
                        fechaPublicacion = datePublication;
                        counter++;
                        break;

                    case 10:


                        Date dateClose = stringToDate(formatStringDate(data.text()));
                        fechaCiereOferta = dateClose;
                        counter++;
                        break;

                    case 11:

                        tipoProceso = data.text();
                        counter++;
                        break;

                    case 12:


                        Date dateFinalization = stringToDate(formatStringDate(data.text()));
                        fechaFinalizacion = dateFinalization;
                        counter++;
                        break;

                    case 13:

                        estatus = data.text();
                        PageInfo pageInfo = new PageInfo(licitationCode, description, modalidad, categoria,
                                tipoConcurso, entidad, tipoEntidad, unidadCompradora, recepcionOfertas,
                                fechaPublicacion, fechaCiereOferta, tipoProceso, fechaFinalizacion, estatus);
                        pageInfoService.save(pageInfo);
                        arrayPages.add(pageInfo);
                        break;

                    default:
                        break;
                }

                for (PageInfo p : arrayPages) {
                    System.out.println("Printing array of objects");
                    System.out.println("==================================");
                    p.print();
                    System.out.println("==================================");
                }
            }
        }
    }

    public Date stringToDate (String stringDate) {

        Date date = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MMMMM.yyyy HH:mm:ss aaa");

        try {

            date = dateFormat.parse(stringDate);

        }  catch (ParseException ex) {

            ex.printStackTrace();
        }
        return date;
    }

    public String formatStringDate (String str) {

        String lastString = "";
        String newString = str.replace(" Hora:", " ");

        if (newString.charAt(newString.length()-5) == 'a') {
            lastString = newString.replace("a. m.", "AM");
        } else {
                lastString = newString.replace("p. m.", "PM");

        }

        return lastString;
    }
}

