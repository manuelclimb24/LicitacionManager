package com.guatecompras.LicitacionManger;

import com.guatecompras.LicitacionManger.scrap.GetDocument;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import javax.annotation.PostConstruct;

@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
@SpringBootApplication
public class LicitacionMangerApplication {

	public LicitacionMangerApplication(GetDocument getDocument) {
		this.getDocument = getDocument;
	}

	public static void main(String[] args) {
		SpringApplication.run(LicitacionMangerApplication.class, args);
	}

	private final GetDocument getDocument;





	@PostConstruct
	public void runScrape() {

		getDocument.selectAllLicitations();
		getDocument.scrappeInfoLicitation();
	}
}
