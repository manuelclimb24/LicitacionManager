package com.guatecompras.LicitacionManger.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Manuel on Sep, 2017
 */
@MappedSuperclass
@Getter
@Setter
public abstract class AbstractEntity implements Serializable, Cloneable {

    @Id
    private String id;

    @JsonIgnore
    private boolean enabled;


    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date createdAt;


    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date updatedAt;

    public AbstractEntity() {
        this.createdAt = new Timestamp(System.currentTimeMillis());
        this.enabled = true;
    }


    @Override
    public int hashCode() {
        int hash = 6;
        hash = 24 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (this.id == null) return false;
        if (obj instanceof AbstractEntity && obj.getClass().equals(getClass())) {
            return this.id.equals(((AbstractEntity) obj).id);
        }
        return super.equals(obj);
    }
}
