package com.guatecompras.LicitacionManger.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "pageinfos")
public class PageInfo extends AbstractEntity{

    private String licitationCode = "";
    private String description = "";
    private String modalidad = "";
    private String categoria = "";
    private String tipoConcurso = "";
    private String entidad = "";
    private String tipoEntidad = "";
    private String unidadCompradora = "";
    private String recepcionOfertas = "";
    private Date fechaPublicacion = new Date();
    private Date fechaCierreOferta = new Date();
    private String tipoProceso = "";
    private Date fechaFinalizacion = new Date();
    private String estatus = "";

    public void print() {

        System.out.println("Codigo de licitacion: " + this.licitationCode);
        System.out.println("Descripcion: " + this.description);
        System.out.println("Modalidad: " + this.modalidad);
        System.out.println("Categoria: " + this.categoria);
        System.out.println("Tipo de concurso: " + this.tipoConcurso);
        System.out.println("Entidad: " + this.entidad);
        System.out.println("Tipo de entidad: " + this.tipoEntidad);
        System.out.println("Unidad compradora: " + this.unidadCompradora);
        System.out.println("Recepcion de ofertas: " + this.recepcionOfertas);
        System.out.println("Fecha de publicacion: " + this.getFechaPublicacion().toString());
        System.out.println("Fecha de cierre de oferta: " + getFechaCierreOferta().toString());
        System.out.println("Tipo de proceso: " + this.tipoProceso);
        System.out.println("Fecha de finalizacion: " + this.getFechaFinalizacion().toString());
        System.out.println("Estatus: " + this.estatus);

    }
}
