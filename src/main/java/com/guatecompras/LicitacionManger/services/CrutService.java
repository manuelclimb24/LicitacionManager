package com.guatecompras.LicitacionManger.services;

import java.util.Collection;
import java.util.Optional;

public interface CrutService<T> {

    Collection<T> findAll();

    Optional<T> findById(String id);

    Optional<T> save(T entity);

    Optional<T> update(String id, T entityUpdate);

    Optional<T> delete(String id);


}
