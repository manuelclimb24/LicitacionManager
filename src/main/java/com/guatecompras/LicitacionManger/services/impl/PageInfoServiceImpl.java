package com.guatecompras.LicitacionManger.services.impl;

import com.guatecompras.LicitacionManger.domain.PageInfo;
import com.guatecompras.LicitacionManger.repositories.PageInfoRepository;
import com.guatecompras.LicitacionManger.services.PageInfoService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;



@Service
public class PageInfoServiceImpl implements PageInfoService {

    private final PageInfoRepository pageInfoRepository;

    public PageInfoServiceImpl(PageInfoRepository pageInfoRepository) {
        this.pageInfoRepository = pageInfoRepository;
    }

    @Override
    public Collection<PageInfo> findAll() {
        return pageInfoRepository.findAll();

    }

    @Override
    public Optional<PageInfo> findById(String id) {
        return null;
    }

    @Override
    public Optional<PageInfo> save(PageInfo entity) {
        return Optional.ofNullable(pageInfoRepository.save(entity));
    }

    @Override
    public Optional<PageInfo> update(String id, PageInfo entityUpdate) {
        return null;
    }

    @Override
    public Optional<PageInfo> delete(String id) {
        return null;
    }
}
