package com.guatecompras.LicitacionManger.controller;


import com.guatecompras.LicitacionManger.domain.PageInfo;
import com.guatecompras.LicitacionManger.services.PageInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/pageinfos")
public class PageInfoController {

    private final PageInfoService pageInfoService;

    public PageInfoController(PageInfoService pageInfoService) {
        this.pageInfoService = pageInfoService;
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public Collection<PageInfo> getAll () {
        return pageInfoService.findAll();
    }
}
