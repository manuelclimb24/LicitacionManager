package com.guatecompras.LicitacionManger.configurations;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@ComponentScan(basePackages = "com.guatecompras.LicitacionManger")
@EnableMongoRepositories(basePackages = "com.guatecompras.LicitacionManger.repositories")
@Configuration
public class FongoConfig extends AbstractMongoConfiguration {
    @Override
    protected String getDatabaseName() {
        return "Test";
    }


    @Bean
    @Override
    public Mongo mongo() throws Exception {
        return new Fongo("Mongo Test").getMongo();
    }
}
