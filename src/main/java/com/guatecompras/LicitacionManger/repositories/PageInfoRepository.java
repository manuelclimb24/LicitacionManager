package com.guatecompras.LicitacionManger.repositories;

import com.guatecompras.LicitacionManger.domain.PageInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface PageInfoRepository extends AbstractEntityRepository<PageInfo> {
}
