package com.guatecompras.LicitacionManger.repositories;

import com.guatecompras.LicitacionManger.domain.AbstractEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AbstractEntityRepository<T extends AbstractEntity> extends MongoRepository<T, String> {

    @Query("{enabled: true,'id': ?0}")
    T findByld (String id);



}
